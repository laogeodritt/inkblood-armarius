---
title: "KazTron Manual - Versions"
summary: "Links to all available manuals for KazTron releases."
last_updated: 30 April 2018
toc: false
---

<div class="btn-group btn-group-lg" role="group">
    <a href="/kaztron/dev" class="btn btn-danger">dev</a>
    <!--<a href="/kaztron/21" class="btn btn-primary">v2.1</a>-->
</div>
