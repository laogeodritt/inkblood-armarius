---
title: "KazTron - Introduction"
last_updated: 21 February 2018
---

{% assign page_url_split = page.url | split: '/' %}
{% capture data_name %}{{page_url_split[1]}}{% endcapture %}
{% assign data = site.data[data_name] %}

Armarius is Inkblood Writing Guild's resident helper bot! It mostly helps the moderators manage the community and run features like BLOTS and the Crit Pit, alongside community features like the Writing Sprints and our Quotes Database.

This website documents the usage of the various commands, organised by module.

Armarius runs an instance of the [KazTron](https://github.com/worldbuilding/kaztron) bot and its modules. Development is concurrent with the KazTron bot on the /r/worldbuilding Discord server.

{% include important.html content="Armarius is designed for use on one Discord server at a time. It is not possible to invite it to other servers: you would need to host it yourself on a separate bot account." %}

## How to use Armarius commands

Armarius is always monitoring all text channels for commands. The structure of a command is as follows:

```
<prefix><command_name> [arg1 [arg2 [arg3...]]]
```

The prefix, in this case `.`, is how Armarius identifies a command intended for it, versus any normal message. This is followed by the command name, a space, and then any number of arguments separated by spaces (very similarly to command line tools, IRC bots, and IRC NickServ/ChanServ commands). If an argument contains spaces, you should enclose it in quotes to ensure it gets interpreted as a single argument.

{% include note.html content="Some commands are restricted based on user account and text channel&mdash;for example, moderator-only commands or `.roll` only usable in #tabletop." %}

{% include tip.html content="If you see your command message instantly disappear, don't panic&mdash;check your PMs! For *some* commands, this is done to avoid channel spam." %}

### Examples

If you want to be notified about World Spotlight events and updates, you can type this command *in any channel*:

```
.spotlight join
```

If you want to roll dice, say three 20-sided dice, you could type the following message in the #tabletop channel (this is a channel-restricted command):

```
.roll 3d20
```

## Getting Help

In addition to this manual, you can always get contextual help from within Discord. To get a list of commands you can use, type `.help` into any channel on the server. To get help on specific commands, you can type `.help <command>`, or even get help with subcommands using `.help <command> <subcommand>`.

For example, `.help spotlight` will give you general information on the spotlight bot features, whereas `.help spotlight join` will specifically tell you about the "join" subcommand and its syntax. (In this case, the commands are both very simple, though!)

{% include tip.html content="Ask for `.help` in the same channel that you want to use commands in. Armarius only shows commands you're allowed to use based on your account and the channel you ask in." %}

## The Team

KazTron is developed and operated by:

{%  assign team = site.data["kaztron-dev"].authors %}

<div class="row">
    {% for author in data.authors.authors %}
    <div class="col-md-4 col-sm-6">
        <div class="panel panel-default nav-panel text-center">
            <div class="panel-heading">
                <!-- TODO: user icons -->
                <span class="fa-stack fa-5x">
                      <i class="fas fa-circle fa-stack-2x text-primary"></i>
                      <i class="fas fa-user fa-stack-1x fa-inverse"></i>
                </span>
            </div>
            <div class="panel-body">
                <h4 class="no-toc no-anchor">{{ author.name }}</h4>
                <p>{{ author.role }}</p>
                {% if author.github != null %}
                <div><a class="icon-link" href="https://github.com/{{ author.github | downcase }}"><i class="fab fa-github"></i> {{author.github}}</a></div>
                {% endif %}
                {% if author.reddit != null %}
                <div><a class="icon-link" href="https://reddit.com/u/{{ author.github | downcase }}"><i class="fab fa-reddit-alien"></i> /u/{{author.reddit}}</a></div>
                {% endif %}
                {% if author.discord != null %}
                <div><i class="fab fa-discord"></i> {{author.discord}}</div>
                {% endif %}
            </div>
         </div>
    </div>
    {% endfor %}
</div>

### Contributors

{% for contributor in data.authors.contributors %}
* **{{ contributor.name }}** {% for link in contributor.links %}<a class="icon-link" href="{{link.url}}" target="_blank" title="{{link.title}}"><i class="{{link.icon}}"></i></a>{% endfor %}: {{contributor.role}}
{% endfor %}

{% include links.html %}
