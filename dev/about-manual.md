---
title: "How to read this manual"
summary: "A guide to how commands are presented in this manual."
last_updated: 21 February 2018
---

Each command will be presented in the following format. The format and fields are described below.

## 1. Command (alias1, alias2)

### 1.1. Sub-command

A general description of what the command does.

In the title, the normal command name, along with aliases and shorthands, are given. The aliases and shorthands can be used in place of the command name: for example, instead of `.command`, you could use `.alias1`.

**Usage:** `.command subcommand <argument1> [argument2 [argument3]]` *(The command name and its arguments, as it would need to be typed. Angle brackets `<arg>` indicates **required** arguments, square brackets `[arg]` indicates **optional** arguments, and a pipe `|` shows allowed alternatives. Never include the brackets when typing your arguments.)*

**Arguments:**
`argument1`
: type. Description of the argument.

`argument2`
: type. Optional. Description of another argument. Default: the default value if not specified

* `argument3`
: type. Optional. Description of the third argument.

**Details**

Further details, if needed.

Users
: Which users can use the command.

Channels
: What channel(s) the command can be used in.

**Examples**

* `.command subcommand potato 1 2`: An example of a command invocation and what it does.
