---
title: "UserStats"
last_updated: 06 August 2018
---

Collects user activity statistics.

    No messages or personally identifiable information are stored by this module, only event counts
    such as number of messages in each channel on an hour-by-hour basis.

## 1. report
{: #report }

**Usage**: `.report <type_> [channel] [daterange]`

<pre>[MOD ONLY] Generate and show a statistics report for a date or range of dates.

If a range of dates is specified, the data retrieved is up to and EXCLUDING the second date.
A day starts at midnight UTC.

The date range cannot cross the boundary of one month (because unique users are not tracked
from month to month for anonymisation reasons; it&#x27;s only possible to identify unique users
within the same month).

This will read and process the raw data to generate stats, and could take some time. Please
avoid calling this function multiple times for the same data or requesting giant ranges.

The file is compressed using gzip. Windows users should use a modern archiving programme
like 7zip &lt;https://www.7-zip.org/download.html&gt;; macOS users can open these files
natively. Linux users know the drill.

Arguments:
* type: One of &quot;full&quot;, &quot;weekday&quot; or &quot;hourly&quot;. &quot;weekday&quot; and &quot;hourly&quot; take the raw data and
    provide a breakdown by day of the week or hour of the day, respectively.
* channel: The name of a channel on the server, or &quot;all&quot;.
* daterange. Optional. This can be a single date (period of 24 hours), or a range of
  dates in the form `date1 to date2`. Each date can be specified as ISO format
  (2018-01-12), in English with or without abbreviations (12 Jan 2018), or as relative dates
  (5 days ago). Default is last month.

Examples:
.report full all 2018-01-12
.report full all yesterday
.report full #general 2018-01-12 to 2018-01-14
.report weekday all 3 days ago to yesterday
.report hourly #worldbuilding 2018-01-01 to 7 days ago</pre>

## 2. userstats
{: #userstats }

**Usage**: `.userstats [daterange]`

<pre>[MOD ONLY] Retrieve a CSV dump of stats for a date or range of dates.

If a range of dates is specified, the data retrieved is up to and EXCLUDING the second date.
A day starts at midnight UTC.

Note that if the range crosses month boundaries (e.g. March to April), then the unique user
hashes can be correlated between each other only within a given month. The same user will
have different hashes in different months. This is used as a anonymisation method, to avoid
long-term tracking of a unique user.

This will generate and upload a CSV file, and could take some time. Please avoid calling
this function multiple times for the same data or requesting giant ranges.

The file is compressed using gzip. Windows users should use a modern archiving programme
like 7zip &lt;https://www.7-zip.org/download.html&gt;; macOS users can open these files
natively. Linux users know the drill.

Arguments:
* daterange. Optional. This can be a single date (period of 24 hours), or a range of
  dates in the form `date1 to date2`. Each date can be specified as ISO format
  (2018-01-12), in English with or without abbreviations (12 Jan 2018), or as relative dates
  (5 days ago). Default is last month.

Examples:
.userstats 2018-01-12
.userstats yesterday
.userstats 2018-01-12 to 2018-01-14
.userstats 3 days ago to yesterday
.userstats 2018-01-01 to 7 days ago</pre>