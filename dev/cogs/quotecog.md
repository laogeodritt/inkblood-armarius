---
title: "QuoteCog"
last_updated: 06 August 2018
---

The Quotes Database helps you capture the best moments on the server! Store your fellow members'
    funniest moments so that you can revisit them time and time again.

## 1. quote (quotes)
{: #quote }

**Usage**: `.[quote|quotes] <user> [number]`

<pre>Retrieve a quote.

If a quote number isn&#x27;t given, find a random quote.

Arguments:
* user: Required. The user to find a quote for. Example formats:
    * @mention of the user (make sure it actually links them)
    * User&#x27;s name + discriminator: JaneDoe#0921
    * Discord ID number: 123456789012345678
* number: Optional. The ID number of the quote to delete (starting from 1), as shown by
    the `.quote` or `.quote list` commands.

Examples:
    .quote @JaneDoe - Find a random quote by JaneDoe.
    .quote @JaneDoe 4 - Find the 4th quote by JaneDoe.</pre>

### 1.1. quote find
{: #quote-find }

**Usage**: `.quote find <user> [search]`

<pre>Find the most recent quote matching a user and/or text search.

Arguments:
* user: Required. The user to find a quote for, or part of their name or nickname to search,
    or &quot;all&quot;. For exact user matches, see `.help quote` for valid formats.
* search: Optional. Text to search in the quote.

Examples:
    .quote find Jane - Find a quote for a user whose user/nickname contains &quot;Jane&quot;.
    .quote find @JaneDoe flamingo - Find a quote containing &quot;flamingo&quot; by JaneDoe.
    .quote find Jane flamingo - Find a quote matching user &quot;Jane&quot; and containing &quot;flamingo&quot;.</pre>

### 1.2. quote list
{: #quote-list }

**Usage**: `.quote list <user> [page]`

<pre>Retrieve a list of quotes. Reply is always PMed.

Arguments:
* user: Required. The user to find a quote for. See `.help quote` for valid formats.
* page: Optional. The page number to access, if there are more than 1 pages of notes.
  Default: last page.

Examples:
    .quote list @JaneDoe - List all quotes by JaneDoe (page 1 if multiple pages)..
    .quote list @JaneDoe 4 - List the 4th page of quotes by JaneDoe.</pre>

### 1.3. quote add
{: #quote-add }

**Usage**: `.quote add <user> <message>`

<pre>Add a new quote manually.

You can use `.quote grab` instead to automatically grab a recent message.

Arguments:
* user: Required. The user to find a quote for. See `.help quote` for valid formats.
* message: Required. The quote text to add.

Examples:
    .quote add @JaneDoe Ready for the mosh pit, shaka brah.</pre>

### 1.4. quote grab
{: #quote-grab }

**Usage**: `.quote grab <user> [search]`

<pre>Find the most recent matching message and add it as a quote.

This command searches the most recent messages (default 100 messages). The most recent
message matching both the user and (if specified) search text is added as a quote.

You can use `.quote add` instead to manually add the quote.

Arguments:
* user: Required. The user to find a quote for. See `.help quote` for valid formats.
* search: Optional. The quote text to find among the user&#x27;s recent messages.

Examples:
    .quote grab @JaneDoe
        Quote the most recent message from @JaneDoe.
    .quote grab @JaneDoe mosh pit
        Finds the most recent message from @JaneDoe containing &quot;mosh pit&quot;.</pre>

### 1.5. quote rem
{: #quote-rem }

**Usage**: `.quote rem <number>`

<pre>Remove one of your own quotes.

THIS COMMAND CANNOT BE UNDONE.

This command is limited to quotes attributed to you. For any other situations, please
contact the moderators to delete quotes.

Arguments:
* number: Optional. The ID number of the quote to delete (starting from 1), as shown by
    the `.quote` or `.quote list` commands.

Examples:
    .quote del 4 - Delete the 4th quote attributed to you.</pre>

### 1.6. quote undo
{: #quote-undo }

**Usage**: `.quote undo`

<pre>Remove the last quote you added.

THIS COMMAND CANNOT BE UNDONE.

This command only undoes `.quote add` or `.quote grab` actions. It does NOT undo
`.quote rem` actions.</pre>

### 1.7. quote del
{: #quote-del }

**Usage**: `.quote del <user> <number>`

<pre>[MOD ONLY] Delete one or all quotes attributed to a user.

Arguments:
* user: Required. The user to find a quote for. See `.help quote` for valid formats.
* number: Required. The ID number of the quote to delete (starting from 1), or &quot;all&quot;.

Examples:
    .quote rem @JaneDoe 4 - Delete the 4th quote by JaneDoe.
    .quote rem @JaneDoe all - Remove all quotes by JaneDoe.</pre>