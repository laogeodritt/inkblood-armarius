---
title: "CheckInManager"
last_updated: 06 August 2018
summary: "Manage check-ins for Inkblood's BLOTS programme."
---

This module allows ArmariusTest to manage user check-ins for Inkblood's BLOTS programme, and
provides tools to help moderators oversee this programme.

## 1. checkin
{: #checkin }

BLOTS weekly check-in.

Enter your **total** word (or page) count and a brief update message.

If your project type is "words", enter your word_count in words (total). If your project
type is "visual" or "script", enter your total number of pages instead. See also
<a href="./checkinmanager.html#checkin-type">checkin type</a>.

**Usage**: `.checkin <word_count> <message>`

**Arguments**

&lt;word_count&gt;
: number. Your total word count (or total pages, depending on set project type). Do **not** include the word 'words' or 'pages'.


&lt;message&gt;
: string. Your progress update. Maximum length 1000 characters.




**Details**

Channels
: #specific.


**Example**

* `.checkin 304882 Finished chapter 82 and developed some of the social and economic fallout of the Potato Battle of 1912.`

### 1.1. checkin type
{: #checkin-type }

Check or set project type for check-ins.

If no argument is provided, checks your project type. If an argument is provided, sets
the project type to the specified value.

This command determines the unit for the word_count you enter when you check in. If your
project type is "words" (the default), enter it in words. If your project type is
"visual" or "script", enter it in pages. See also <a href="./checkinmanager.html#checkin">checkin</a>.

**Usage**: `.checkin type [project_type]`

**Arguments**

[project_type]
: `words`, `visual` or `script`. Optional. The project type to change to.




**Details**

Channels
: #specific.


**Examples**

* `.checkin type` - Check your current project type.
* `.checkin type script` - Set your project type to script.

### 1.2. checkin list
{: #checkin-list }

Check your list of check-ins.

The result is always PMed to you.

Moderators can query any user's checkins with <a href="./checkinmanager.html#checkin-query">checkin query</a> instead.

**Usage**: `.checkin list [page]`

**Arguments**

[page]
: number. Optional. The page number to access, if a user has more than 1 page of badges. Default: last page (most recent)




**Details**

Channels
: #specific.


**Examples**

* `.checkin list` - List all your check-ins (last page if multiple pages).
* `.checkin list 4` - List the 4th page of check-ins.

### 1.3. checkin report
{: #checkin-report }

Get a report of who has or has not checked in in a given week.

**Usage**: `.checkin report [datespec]`

**Arguments**

[datespec]
: datespec. Optional. A date in any unambiguous format (2018-03-14, March 14 2018, 14 March 2018, today, 1 month ago, etc.). The report will be for the check-in week that includes this date. Default: last week ("7 days ago")




**Details**

Members
: Moderators, Administrators.


Channels
: Mod channels.


**Examples**

* `.checkin report` - Get a report for last week.
* `.checkin report 2018-04-18` - Get a report for the week that includes 18 April 2018.

### 1.4. checkin query
{: #checkin-query }

Query a user's list of check-ins.

This is the moderator's version of <a href="./checkinmanager.html#checkin-list">checkin list</a>.

**Usage**: `.checkin query <user> [page]`

**Arguments**

&lt;user&gt;
: @mention. The user to check (as an @mention or a Discord ID).


[page]
: number. Optional. The page number to access, if a user has more than 1 page of badges. Default: last page (most recent)




**Details**

Members
: Moderators, Administrators.


**Examples**

* `.checkin query @JaneDoe` - List all check-ins by JaneDoe (last page if multiple pages).
* `.checkin query @JaneDoe 4` - List the 4th page of check-ins by JaneDoe.

### 1.5. checkin exempt
{: #checkin-exempt }

Check or set exemptions from check-ins.

Users who are exempt from check-ins will not appear in a <a href="./checkinmanager.html#checkin-report">checkin report</a>.

**Usage**: `.checkin exempt [user] [val]`

**Arguments**

[user]
: @mention. Optional. The user to check (as an @mention or a Discord ID).


[val]
: "yes" or "no". Optional. If not specified, check a user's exemption status. If specified, change that user's exemption status.




**Details**

Members
: Moderators, Administrators.


Channels
: Mod channels.


**Examples**

* `.checkin exempt` - Get a list of exempt users.
* `.checkin exempt @JaneDoe` - Check if JaneDoe is exempt from check-ins.
* `.checkin exempt @JaneDoe yes` - Set JaneDoe as exempt from check-ins.

## 2. milestone
{: #milestone }

Command group for milestone management tools.

**Usage**: `.milestone`

**Details**

Members
: Moderators, Administrators.


Channels
: Mod channels.


### 2.1. milestone report
{: #milestone-report }

Give a report of each user's current milestone roles compared to their last check-in.

**Usage**: `.milestone report`

**Details**

Members
: Moderators, Administrators.


Channels
: Mod channels.


### 2.2. milestone update
{: #milestone-update }

Update a user's milestone role.

**Usage**: `.milestone update <user>`

**Arguments**

&lt;user&gt;
: @mention. The user to update (as an @mention or a Discord ID).




**Details**

Members
: Moderators, Administrators.
