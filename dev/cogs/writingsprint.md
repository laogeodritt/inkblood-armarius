---
title: "WritingSprint"
last_updated: 06 August 2018
---

Welcome to writing sprints, where everything's made up and the words don't matter!

    For help with this feature, please type `.help sprint`.

## 1. sprint (w)
{: #sprint }

**Usage**: `.[sprint|w]`

<pre>Welcome to writing sprints, where everything&#x27;s made up and the words don&#x27;t matter!

In writing sprints (a.k.a. word wars), you get together with a group of other server members
and write for a fixed amount of time, usually 15 or 30 minutes, on whatever project you
choose.

At the end of the sprint, you report your word count, and whoever wrote the most wins!
Not that they matter. Because you got some writing done. So you&#x27;re always a winner.

Writing sprints are a great way of getting you to focus on your writing with a group of
other people. And, y&#x27;know, not just chatting with them the entire time when you told
yourself you&#x27;d get some writing done this evening.

Get writing!</pre>

### 1.1. sprint status (?)
{: #sprint-status }

**Usage**: `.sprint [status|?]`

<pre>Get the current status of the sprint.</pre>

### 1.2. sprint start (s)
{: #sprint-start }

**Usage**: `.sprint [start|s] [duration] [delay]`

<pre>Start a new sprint.

After starting the sprint, you need to join the sprint with .w join in order to specify your
initial wordcount.

Arguments:
* duration: Optional. The amount of time, in minutes, for the sprint to last.
  Default: 25 minutes.
* delay: Optional. The amount of time, in minutes, to wait before starting the sprint.
  Default: 5 minutes.

Examples:
    .w start - Create a 25 minute sprint, starting in 5 minutes.
    .w start 15 - Create a 15-minute sprint, starting in 5 minutes.
    .w start 25 1 - Create a 25-minute sprint, starting in 1 minute.</pre>

### 1.3. sprint stop (x, cancel)
{: #sprint-stop }

**Usage**: `.sprint [stop|x|cancel]`

<pre>Cancel the current sprint.

This can only be done by the creator of the sprint or moderators, and only if a sprint is
ongoing or is about to start.</pre>

### 1.4. sprint join (j)
{: #sprint-join }

**Usage**: `.sprint [join|j] <wordcount>`

<pre>Join a sprint and set your initial wordcount.

You can also use this command to fix your initial wordcount, if you made a mistake when
initially joining the sprint.

This will only work if a sprint is ongoing or has been created with .w start.

Arguments:
* &lt;wordcount&gt;: Required. Your initial wordcount, before the start of the sprint. When you
  report your wordcount at the end of the sprint, your total words written during the sprint
  will automatically be calculated.

Example:
    .w join 12044 - Join the sprint with an initial wordcount of 12,044 words.</pre>

### 1.5. sprint leave (l)
{: #sprint-leave }

**Usage**: `.sprint [leave|l]`

<pre>Leave a sprint you previously joined.

You should normally only need to use this if you realise you can&#x27;t stay for the entire
sprint, or otherwise can&#x27;t participate in the sprint.</pre>

### 1.6. sprint wordcount (wc, c)
{: #sprint-wordcount }

**Usage**: `.sprint [wordcount|wc|c] <count>`

<pre>Report your wordcount.

If used before a sprint starts, this changes your starting wordcount. During or after a
sprint, it sets your current wordcount and calculates how much you&#x27;ve written during the
sprint.

If you&#x27;re setting your final wordcount for the end of the sprint, make sure to use the
`.w final` command.

Arguments:
* &lt;wordcount&gt;: Required. Your final wordcount at the end of the sprint. The bot will
  automatically calculate your total words written during the sprint.

Example:
    .w c 12888 - Report that your wordcount is 12888.</pre>

### 1.7. sprint final
{: #sprint-final }

**Usage**: `.sprint final`

<pre>Finalize your wordcount. Use this when you&#x27;re sure you&#x27;re done and your wordcount is
correct.</pre>

### 1.8. sprint leader
{: #sprint-leader }

**Usage**: `.sprint leader [date]`

<pre>Show the leaderboards.

Arguments:
* [date]: Optional. Various date formats are accepted like 2018-03-14, 14 Mar 2018,
  yesterday. If not given, shows leaderboard for all time; if specified, shows leaderboard
  for the week that includes the given date.

Examples:
    .w leader - All-time leaderboard.
    .w leader 2018-03-14 - Leaderboard for the week that contains 14 March 2018.</pre>

### 1.9. sprint stats
{: #sprint-stats }

**Usage**: `.sprint stats <user> [date]`

<pre>Show stats, either global or per-user.

Arguments:
* &lt;user&gt;: An @mention of the user to look up, or &quot;all&quot; for global stats.
* [date]: Optional. Various date formats are accepted like 2018-03-14, 14 Mar 2018,
  yesterday. If not given, shows stats for all time; if specified, shows stats
  for the week that includes the given date.

Examples:
    .w stats all - Global stats for all time.
    .w stats @JaneDoe - Stats for JaneDoe for all time.
    .w stats all 2018-03-14 - Global stats for the week including 14 March.</pre>

### 1.10. sprint statreset
{: #sprint-statreset }

**Usage**: `.sprint statreset [user]`

<pre>Reset your own stats (or any user&#x27;s stats, for mods).

THIS CANNOT BE UNDONE.

Resetting one user&#x27;s stats will not affect global stats.

Arguments:
* [user]: Optional, for mods only. Reset another user&#x27;s stats. Can be an @mention of another
  user, &quot;global&quot; or &quot;all&quot;.

Examples:
    .w stats_reset - Reset your own stats.
    .w stats_reset @JaneDoe - Reset Jane Doe&#x27;s stats (mods only).
    .w statsreset global - Reset global stats only (user stats are preserved).</pre>

### 1.11. sprint follow
{: #sprint-follow }

**Usage**: `.sprint follow`

<pre>Get notified when sprints are happening.</pre>

### 1.12. sprint unfollow
{: #sprint-unfollow }

**Usage**: `.sprint unfollow`

<pre>Stop getting notifications about sprints.

You will still get notifications for sprints you have joined.</pre>