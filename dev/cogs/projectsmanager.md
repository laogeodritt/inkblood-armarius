---
title: "ProjectsManager"
last_updated: 06 August 2018
summary: "Share your projects with other members!"
---

The Projects module lets members share their projects with each other! With this module,
they can set up a basic member profile; set up projects with basic info and a summary;
look up each others' projects; and follow each other's project notification roles.

Projects are output to #specific, in addition to being browsable via the
<a href="./projectsmanager.html#project">project</a>, <a href="./projectsmanager.html#project-search">project search</a>, etc. commands.

If roles are set up for genres and project types, this module is able to manage roles
according to the current active project for each user.

## Configuration

`projects` configuration section:

`project_channel`
: channel ID (18-digit numeric). Channel in which to output/archive projects.

`max_projects`
: number. Maximum number of projects. This can be overridden on a per-user basis with
  <a href="./projectsmanager.html#project-admin-limit">project admin limit</a>.

`max_projects_map`
: Dict. Associates role names to max projects. Takes priority over the `max_projects`
  configuration value, but is superseded by per-user limits.

`timeout_confirm`
: number. Amount of time (in seconds) to confirm certain actions like delete.

`timeout_wizard`
: number. Inactivity (in seconds) before a wizard times out (cancels itself).

## Initial set-up

Once this module is loaded and running in the bot, you must set up the genre and type lists
from within Discord. See <a href="./projectsmanager.html#project-admin-genre">project admin genre</a> and <a href="./projectsmanager.html#project-admin-type">project admin type</a>.

## 1. project (projects)
{: #project }

Show project information or a list of a user's projects.

If no user or project name is specified, this command shows your own projects.

If no name is specified, this command shows a list of the user's projects and their
currently active project.

If a name is specified, this command shows that project's information.

**Usage**: `.[project|projects] [member] [name]`

**Arguments**

[member]
: @user. Optional. The user to look up. Default: yourself


[name]
: string. Optional. Part of a title to look up. One word or a substring is fine.




**Examples**

* `.project` - Get a list of your own projects.
* `.project @JaneDoe#0921` - Get Jane Doe's list of projects and currently active project.
* `.project @JaneDoe#0921 flaming` - Get info on Jane Doe's project with 'flaming' in the title.

### 1.1. project search
{: #project-search }

Search projects by genre or type, or within title and body text.

Title and body text searches are case-insensitive.

**Usage**: `.project search <search>`

**Arguments**

&lt;search&gt;
: string with keywords ("genre", "type", "title"). What to search. Text entered here will search the body text (that is, title + elevator pitch + description). You can include keyword arguments ("genre", "type" and "title") at the **beginning** of the search string (see examples).


&lt;genre (keyword)&gt;
: string. The genre name. This must exactly match an item in the list of genres.


&lt;type (keyword)&gt;
: string. The project type. This must exactly match an item in the list of types.


&lt;title (keyword)&gt;
: string. Search string in the title.




**Examples**

* `.project search flamingo` - Find all projects that contain 'flamingo' in their title, description or pitch.
* `.project search title="flamingo"` - Find all projects that contain 'flamingo' in their title.
* `.project search genre="fantasy" flamingo` - Find all projects in the fantasy genre that contain the word 'flamingo' in their title, description or pitch.
* `.project search genre="fantasy" title="flamingo" grapefruit` - Find all fantasy projects with 'flamingo' in the title and 'grapefruit' in the body text.

### 1.2. project select
{: #project-select }

Select a project to mark as your 'active' project.

This project will be shown by default when someone looks you up. This is also the
project that will be modified by project-editing commands like <a href="./projectsmanager.html#project-wizard">project wizard</a>,
<a href="./projectsmanager.html#project-title">project title</a>, etc.

**Usage**: `.project select <name>`

**Arguments**

&lt;name&gt;
: string. Part of the project's title. One word or a substring is fine.




**Example**

* `.project select Gale`

### 1.3. project follow
{: #project-follow }

Follow a project.

This adds you to the project's followable role, and allows you to get @mention'd in
relationship to the project. This can be used for news, for the author to open
discussions on their project, etc. - exact usage will depend on the specific Discord
server's community and rules.

Not all projects have a follow role. To check, look up the project with <a href="./projectsmanager.html#project">project</a>, or
check the full list of followable projects with <a href="./projectsmanager.html#project-followable">project followable</a>.

Use <a href="./projectsmanager.html#project-unfollow">project unfollow</a> to un-follow a project.

**Usage**: `.project follow <member> [name]`

**Arguments**

&lt;member&gt;
: @user. The user to look up.


[name]
: string. Optional. Part of the project's title. One word or a substring is fine. Default: user's active project




**Example**

* `.project follow @JaneDoe#0921 flamingo`

### 1.4. project unfollow
{: #project-unfollow }

Un-follow a project.

See <a href="./projectsmanager.html#project-follow">project follow</a> for more information on followable projects.

**Usage**: `.project unfollow <member> [name]`

**Arguments**

&lt;member&gt;
: @user. The user to look up.


[name]
: string. Optional. Part of the project's title. One word or a substring is fine. Default: user's active project




**Example**

* `.project unfollow @JaneDoe#0921 flamingo`

### 1.5. project followable (followables)
{: #project-followable }

List all projects that can be followed.

See <a href="./projectsmanager.html#project-follow">project follow</a> for more information on followable projects.

**Usage**: `.project [followable|followables]`

**Example**

* `.project followable`

### 1.6. project new
{: #project-new }

Create a new project using the wizard.

ArmariusTest will PM you with a series of questions to answer to set up the basics of your
project. Make sure you have PMs enabled.

**Usage**: `.project new`

**Details**

{% include warning.html content='If you don&#x27;t respond for more than 30 minutes, the new
project command will automatically be cancelled.' %}

{% include tip.html content='You can specify more information about your project, like a URL and extended
description, using <a href="./projectsmanager.html#project-title">project title</a>, <a href="./projectsmanager.html#project-genre">project genre</a>, <a href="./projectsmanager.html#project-subgenre">project subgenre</a>,
<a href="./projectsmanager.html#project-type">project type</a>, <a href="./projectsmanager.html#project-pitch">project pitch</a>, <a href="./projectsmanager.html#project-url">project url</a>, <a href="./projectsmanager.html#project-description">project description</a>.' %}

**Example**

* `.project new`

### 1.7. project wizard
{: #project-wizard }

Edit your active project using the wizard.

Your active project is the one set with <a href="./projectsmanager.html#project-select">project select</a>.

ArmariusTest will PM you with a series of questions to answer to set up the basics of your
project. Make sure you have PMs enabled.

**Usage**: `.project wizard`

**Details**

{% include warning.html content='If you don&#x27;t respond for more than 30 minutes, the new
project command will automatically be cancelled.' %}

{% include tip.html content='You can specify more information about your project, like a URL and extended
description, using (<a href="./projectsmanager.html#project-title">project title</a>, <a href="./projectsmanager.html#project-genre">project genre</a>, <a href="./projectsmanager.html#project-subgenre">project subgenre</a>,
<a href="./projectsmanager.html#project-type">project type</a>, <a href="./projectsmanager.html#project-pitch">project pitch</a>, <a href="./projectsmanager.html#project-url">project url</a>, <a href="./projectsmanager.html#project-description">project description</a>).' %}

**Example**

* `.project wizard`

### 1.8. project aboutme
{: #project-aboutme }

Set up your author profile with the wizard.

ArmariusTest will PM you with a series of questions to answer to set up the basics of your
project. Make sure you have PMs enabled.

**Usage**: `.project aboutme`

**Details**

{% include warning.html content='If you don&#x27;t respond for more than 30 minutes, the new
project command will automatically be cancelled.' %}

**Example**

* `.project aboutme`

### 1.9. project cancel
{: #project-cancel }

Cancel any open wizard.

Wizards are started by commands like <a href="./projectsmanager.html#project-new">project new</a>, <a href="./projectsmanager.html#project-wizard">project wizard</a> and
<a href="./projectsmanager.html#project-aboutme">project aboutme</a>.

**Usage**: `.project cancel`

**Example**

* `.project cancel`

### 1.10. project delete
{: #project-delete }

Delete one of your projects.

ArmariusTest will send a message with project info. Use the emoji reactions to confirm or
cancel. The request will auto-cancel if you do not respond within a few minutes.

**Usage**: `.project delete [name]`

**Arguments**

[name]
: string. Optional. Part of the project's title. One word or a substring is fine. Default: your active project




**Example**

* `.project delete flamingo`

### 1.11. project title
{: #project-title }

Set the active project's title.

The active project can be set with <a href="./projectsmanager.html#project-select">project select</a>.

**Usage**: `.project title [new_value]`



### 1.12. project genre
{: #project-genre }

Set the active project's genre.

The active project can be set with <a href="./projectsmanager.html#project-select">project select</a>.

**Usage**: `.project genre [new_value]`



### 1.13. project subgenre
{: #project-subgenre }

Set the active project's subgenre.

The active project can be set with <a href="./projectsmanager.html#project-select">project select</a>.

**Usage**: `.project subgenre [new_value]`



### 1.14. project type
{: #project-type }

Set the active project's type.

The active project can be set with <a href="./projectsmanager.html#project-select">project select</a>.

**Usage**: `.project type [new_value]`



### 1.15. project pitch
{: #project-pitch }

Set the active project's elevator pitch.

The active project can be set with <a href="./projectsmanager.html#project-select">project select</a>.

**Usage**: `.project pitch [new_value]`



### 1.16. project url
{: #project-url }

Set the active project's website URL.

The active project can be set with <a href="./projectsmanager.html#project-select">project select</a>.

**Usage**: `.project url [new_value]`



### 1.17. project description
{: #project-description }

Set the active project's long description.

The active project can be set with <a href="./projectsmanager.html#project-select">project select</a>.

**Usage**: `.project description [new_value]`



### 1.18. project admin
{: #project-admin }

Command group for administrative tools.

**Usage**: `.project admin`

**Details**

Members
: Moderators, Administrators.


#### 1.18.1. project admin genre
{: #project-admin-genre }

List all genres.

**Usage**: `.project admin genre`

**Details**

Members
: Moderators, Administrators.


##### 1.18.1.1. project admin genre add
{: #project-admin-genre-add }

Add a new genre.

**Usage**: `.project admin genre add <name> [role]`

**Arguments**

&lt;name&gt;
: string. The name of the genre. Must be unique. Users will have to type this exactly, so keep it short and easy to type. Use quotation marks if the name contains spaces.


[role]
: @role. Optional. @mention of the role to associate to this genre. This role must already exist on the server. Default: None




**Details**

Members
: Moderators, Administrators.


**Examples**

* `.project admin genre add Fantasy` - Add a "Fantasy" genre with no associated role.
* `.project admin genre add Fantasy @Fantasy Writers` - Add a "Fantasy" genre with the role "Fantasy Writers".

##### 1.18.1.2. project admin genre edit
{: #project-admin-genre-edit }

Change an existing genre.

**Usage**: `.project admin genre edit <old_name> <new_name> [new_role]`

**Arguments**

&lt;old_name&gt;
: string. The current name for this genre. Use quotation marks if the name contains spaces.


&lt;new_name&gt;
: string. The new name of the genre. Must be unique. Users will have to type this exactly, so keep it short and easy to type. Use quotation marks if the name contains spaces.


[new_role]
: @role. Optional. @mention of the role to associate to this genre. This role must already exist on the server. Default: None




**Details**

Members
: Moderators, Administrators.


**Example**

* `.project admin genre edit Fantasy "High Fantasy"` - Rename the Fantasy genre to High Fantasy (and remove the role, if it had one - if you want to keep a role you MUSt specify it as a third argument).

##### 1.18.1.3. project admin genre rem
{: #project-admin-genre-rem }

Remove a genre.

This is only allowed if: a) no projects are using this genre, or b) you specify a
replacement genre.

**Usage**: `.project admin genre rem <name> [replace_name]`

**Arguments**

&lt;name&gt;
: string. The current name for this genre. Use quotation marks if the name contains spaces.


[replace_name]
: string. Optional. The name of another existing genre. Any projects using the old genre will be updated to this genre. Use quotation marks if the name contains spaces.




**Details**

Members
: Moderators, Administrators.


**Example**

* `.project admin genre remove "High Fantasy" Fantasy` - Remove the "High Fantasy" genre, and replace any projects using that genre with the "Fantasy" genre.

#### 1.18.2. project admin type
{: #project-admin-type }

List all project types.

**Usage**: `.project admin type`

**Details**

Members
: Moderators, Administrators.


##### 1.18.2.1. project admin type add
{: #project-admin-type-add }

Add a new project type.

**Usage**: `.project admin type add <name> [role]`

**Arguments**

&lt;name&gt;
: string. The name of the project type. Must be unique. Users will have to type this exactly, so keep it short and easy to type. Use quotation marks if the name contains spaces.


[role]
: @role. Optional. @mention of the role to associate to this project type. This role must already exist on the server. Default: None




**Details**

Members
: Moderators, Administrators.


**Examples**

* `.project admin type add Novel` - Add a "Novel" project type with no associated role.
* `.project admin type add Novel @Novelists` - Add a "Novel" project type with the role "Novelists".

##### 1.18.2.2. project admin type edit
{: #project-admin-type-edit }

Change an existing project type.

**Usage**: `.project admin type edit <old_name> <new_name> [new_role]`

**Arguments**

&lt;old_name&gt;
: string. The current name for this project type. Use quotation marks if the name contains spaces.


&lt;new_name&gt;
: string. The new name of the project type. Must be unique. Users will have to type this exactly, so keep it short and easy to type. Use quotation marks if the name contains spaces.


[new_role]
: @role. Optional. @mention of the role to associate to this project type. This role must already exist on the server. Default: None




**Details**

Members
: Moderators, Administrators.


**Example**

* `.project admin type edit "Short Story" Anthology` - Rename the Short Story type to Anthology (and remove the role, if it had one - if you want to keep a role you MUSt specify it as a third argument).

##### 1.18.2.3. project admin type rem
{: #project-admin-type-rem }

Remove a project type.

This is only allowed if: a) no projects are using this project type, or b) you specify a
replacement project type.

**Usage**: `.project admin type rem <name> [replace_name]`

**Arguments**

&lt;name&gt;
: string. The current name for this project type. Use quotation marks if the name contains spaces.


[replace_name]
: string. Optional. The name of another existing project type. Any projects using the old project type will be updated to this one. Use quotation marks if the name contains spaces.




**Details**

Members
: Moderators, Administrators.


**Example**

* `.project admin type remove "Game Script" Script` - Remove the "Game Script" genre, and replace any projects using that genre with the "Script" genre.

#### 1.18.3. project admin limit
{: #project-admin-limit }

Set a maximum number of projects that a user can have.

If the user exceeds this number of projects, their old projects will not be deleted.
However, they will be denied creating new projects until they delete enough projects to
be below this limit.

**Usage**: `.project admin limit <member> <limit>`

**Arguments**

&lt;member&gt;
: @user. The user to look up.


&lt;limit&gt;
: string. New maximum number of projects. Use -1 to set this to the default value (as specified in the configuration file).




**Details**

Members
: Moderators, Administrators.


**Example**

* `.projects admin limit @MultiCoreProcessor#1234 8` - Change the limit to 8 for the user MultiCoreProcessor.

#### 1.18.4. project admin followable
{: #project-admin-followable }

Set a follow role for a project.

The follow role must already exist. It should generally be mentionable so that the
project author is able to notify followers.

**Usage**: `.project admin followable <member> <title> [role]`

**Arguments**

&lt;member&gt;
: @user. The user to look up.


&lt;name&gt;
: string. Part of a title to look up. One word or a substring is fine.


[role]
: @role. Optional. A mention of the role to use as a follow role. If not specified, the follow role is **removed** from the project.




**Details**

Members
: Moderators, Administrators.


**Example**

* `.projects admin followable @JaneDoe#0921 "Potato Mansion" @PotatoMansion`

#### 1.18.5. project admin delete
{: #project-admin-delete }

Delete a user's project.

ArmariusTest will send a message with project info. Use the emoji reactions to confirm or
cancel. The request will auto-cancel if you do not respond within a few minutes.

**Usage**: `.project admin delete <member> [name]`

**Arguments**

&lt;member&gt;
: @user. The user to look up.


[name]
: string. Optional. Part of the project's title. One word or a substring is fine. Default: user's active project




**Details**

Members
: Moderators, Administrators.


**Example**

* `.project delete flamingo`

#### 1.18.6. project admin purge
{: #project-admin-purge }

Purge all projects from users who have left the server.

**Usage**: `.project admin purge`

**Details**

Members
: Moderators, Administrators.


**Example**

* `.projects admin purge`