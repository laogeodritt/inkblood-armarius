---
title: "BadgeManager"
last_updated: 06 August 2018
summary: "Give users badges for community contributions. Part of Inkblood Writing Guild BLOTS."
---

BadgeManager lets users give each other badges for contribution to each others' projects
and to the community.

Badges can be given in the #badges channel. Check the pins in this channel for
instructions. ArmariusTest will detect all messages in this channel and let you know when it
detects that you've given a badge. **Check the ArmariusTest response to make sure your badge
registered properly.**

You can also edit or delete your old message, and ArmariusTest will detect the change and update
its badge list accordingly.

## 1. badges
{: #badges }

Check a user's badges.

{% include tip.html content='If you want to give a badge, leave a properly formatted message in
#badges. See <a href="./badgemanager.html">BadgeManager</a> or `.help BadgeManager` for more information.' %}

**Usage**: `.badges <user> [page]`

**Arguments**

&lt;user&gt;
: @mention. The user to check (as an @mention or a Discord ID).


[page]
: number. Optional. The page number to access, if a user has more than 1 page of badges. Default: last page (most recent)




**Examples**

* `.badge @JaneDoe` - List all of JaneDoe's badges (most recent, if there are multiple pages).
* `.checkin badge @JaneDoe 4` - List the 4th page of JaneDoe's badges.

### 1.1. badges report
{: #badges-report }

Show a report of member badge counts.

**Usage**: `.badges report [min_badges=1]`

**Arguments**

[min_badges]
: number. Optional. Minimum number of badges a user needs to have to be included in the report.




**Details**

Members
: Moderators, Administrators.


Channels
: Mod channels.


### 1.2. badges load
{: #badges-load }

Read the badge channel history and add any missing badges. Mostly useful for transitioning to bot-managed badges, or loading missed badges from bot downtime.

**Usage**: `.badges load [messages=100]`

**Arguments**

[messages]
: number. Optional. Number of messages to read in history. Default: 100




**Details**

Members
: Moderators, Administrators.
